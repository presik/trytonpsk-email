# -*- coding: UTF-8 -*-
# This file is part electronic_mail_template module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import base64
import mimetypes

import requests
from mailtrap import Address, Attachment, Disposition, Mail, MailtrapClient
from mailtrap.mail.base import BaseMail
from trytond.pool import Pool


class MailtrapClientCustom(MailtrapClient):

    def send(self, mail: BaseMail):
        url = f"{self.base_url}/api/send"
        response = requests.post(url, headers=self.headers, json=mail.api_data)
        return response


class MailTrap:

    def __init__(self, api_key):
        self.api = api_key

    def prepare_attachment(self, data, filename, ext):
        if not filename:
            filename = 'test_filename'

        # attachment.content = encoded
        # attachment.filename = _filename
        file_base64 = base64.b64encode(data)
        _filename = filename + '.' + ext
        attachment = Attachment(content=file_base64, filename=_filename)
        content_type, _ = mimetypes.guess_type(_filename)
        attachment.mimetype = content_type
        attachment.disposition = Disposition('attachment')
        attachment.content_id = _filename
        return attachment

    def send(self, template, record, to_recipients=[], attach=False, attachments=[], body=None):
        if not template or (not to_recipients and not template.recipients):
            return
        if body:
            html_content = body
        else:
            html_content = template.engine_jinja2(template.html_body, record, template.webapi.api_response)

        if not to_recipients:
            to_recipients = template.recipients.split(',')

        message = Mail(
            sender=Address(template.from_email),
            to=[Address(recipient) for recipient in to_recipients],
            subject=template.subject,
            html=html_content,
            attachments=[],
        )
        if template.report:
            ext, data, _, file_name = template.render_report(record)
            attachment = self.prepare_attachment(data, file_name, ext)
            message.attachments.append(attachment)

        """
        Diccionary list attachments are:
            [{
                'attachment': <document>,
                'file_name': <filenae>,
                'extension': <xml, jpg>,
            },]
        """
        for att in attachments:
            attachment = self.prepare_attachment(
                att['attachment'], att['file_name'], att['extension'],
            )
            message.attachments.append(attachment)
        if template.bcc:
            message.bcc = [Address(bcc) for bcc in template.bcc.split(',')]

        mt = MailtrapClientCustom(self.api)
        response = mt.send(message)
        if response.status_code in [200, 202]:
            Email = Pool().get('ir.email')
            email = Email(
                recipients=to_recipients,
                recipients_secondary=None,
                recipients_hidden=template.bcc,
                addresses=None,
                subject=template.subject,
                body=html_content,
                resource=record)
            email.save()
        return response

    def _test_send(self, template):
        to_recipients = template.recipients.split(',')
        message = Mail(
            sender=Address(template.from_email),
            to=[Address(recipient) for recipient in to_recipients],
            subject=template.subject,
            html=template.html_body,
        )
        mt = MailtrapClientCustom(self.api)
        response = mt.send(message)

# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from trytond.model import ModelView, ModelSQL, fields


class EmailActivity(ModelSQL, ModelView):
    'Email Activity'
    __name__ = 'email.activity'
    origin = fields.Reference('Origin', selection='get_origin', readonly=True)
    template = fields.Many2One('email.template', 'Template', readonly=True)
    status = fields.Selection([
        ('sended', 'Sended'),
        ('not_delivered', 'Not Delivered'),
        ('delivered', 'Delivered'),
        ],'Status', readonly=True)

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return []

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        models = cls._get_origin()
        models = Model.search([
            ('model', 'in', models),
        ])
        return [('', '')] + [(m.model, m.name) for m in models]

# This file is part electronic_mail_template module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import template
from . import web_service
from . import email_activity


def register():
    Pool.register(
        web_service.WebAPI,
        template.Template,
        email_activity.EmailActivity,
        module='email', type_='model')

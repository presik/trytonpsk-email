# This file is part electronic_mail_template module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class WebAPI(ModelSQL, ModelView):
    'Email WebAPI'
    __name__ = 'email.webapi'
    name = fields.Char('Name', required=True)
    web_service = fields.Selection([
        ('sendgrid', 'Sendgrid'),
        ('mailtrap', 'MailTrap'),
        ], 'Web Service', required=True)
    api_key = fields.Char('Api Key', required=False)
    api_response = fields.Char('Api Response', help='This option is for response emails')

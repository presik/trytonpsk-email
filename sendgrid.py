# -*- coding: UTF-8 -*-
# This file is part electronic_mail_template module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import base64
import logging
import mimetypes

from trytond.pool import Pool

try:
    import html2text
except ImportError:
    html2text = None

from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import (
    Attachment,
    ContentId,
    Disposition,
    FileContent,
    FileName,
    FileType,
    Mail,
)

logger = logging.getLogger(__name__)


class SendGrid:

    def __init__(self, api_key):
        self.api = api_key

    def prepare_attachment(self, data, filename, ext):
        if not filename:
            filename = 'test_filename'

        encoded = base64.b64encode(data).decode()
        attachment = Attachment()
        attachment.file_content = FileContent(encoded)
        _filename = filename + '.' + ext
        content_type, _ = mimetypes.guess_type(_filename)
        attachment.file_type = FileType(content_type)
        attachment.file_name = FileName(_filename)
        attachment.disposition = Disposition('attachment')
        attachment.content_id = ContentId('Example Content ID')
        return attachment

    def send(self, template, record, to_recipients=[], attach=False, attachments=[], body=None):
        if not template or (not to_recipients and not template.recipients):
            return
        if body:
            html_content = body
        else:
            html_content = template.engine_jinja2(template.html_body, record, template.webapi.api_response)
        if not to_recipients:
            to_recipients = template.recipients

        message = Mail(
            from_email=template.from_email,
            to_emails=to_recipients,
            subject=template.subject,
            html_content=html_content,
        )
        if template.report:
            ext, data, _, file_name = template.render_report(record)
            attachment = self.prepare_attachment(data, file_name, ext)
            message.attachment = attachment

        """
        Diccionary list attachments are:
            [{
                'attachment': <document>,
                'file_name': <filename>,
                'extension': <xml, jpg>,
            },]
        """
        for att in attachments:
            attachment = self.prepare_attachment(
                att['attachment'], att['file_name'], att['extension'],
            )
            message.add_attachment(attachment)
        if template.bcc:
            message.add_bcc(template.bcc)

        try:
            sg = SendGridAPIClient(self.api)
            response = sg.send(message)
            if response.status_code in [200, 202]:
                Email = Pool().get('ir.email')
                body_text = None
                if html2text:
                    converter = html2text.HTML2Text()
                    body_text = converter.handle(html_content)
                email = Email(
                    recipients=",".join(to_recipients),
                    recipients_secondary=None,
                    recipients_hidden=template.bcc,
                    addresses=None,
                    subject=template.subject,
                    body=body_text,
                    resource=record)
                email.save()
            print(response.status_code)
            return response
        except Exception as e:
            logger.exception(e)

    def _test_send(self, template):
        message = Mail(
            from_email=template.from_email,
            to_emails=template.recipients,
            subject=template.subject,
            plain_text_content='Quick and easy to do anywhere, even with Python',
            html_content=template.html_body,
        )
        if template.bcc:
            message.add_bcc(template.bcc)
        sg = SendGridAPIClient(self.api)
        response = sg.send(message)
        print(response.status_code)
        print(response.body)

    def get_email_activity(self, message_id):
        try:
            sg = SendGridAPIClient(self.api)
            response = sg.client.messages._(message_id).get()
            print(response.status_code)
            print(response.body)
        except Exception:
            print(e)

    def filter_email_activity(self, params=None):
        try:
            sg = SendGridAPIClient(self.api)
            response = sg.client.messages.get(query_params=params)
            print(response.status_code)
            print(response.body)
        except Exception:
            print(e)

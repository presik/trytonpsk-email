# -*- coding: UTF-8 -*-
# This file is part electronic_mail_template module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from email.mime.multipart import MIMEMultipart
from email.utils import formatdate

from jinja2 import Template as Jinja2Template
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool

from .mailtrap import MailTrap
from .sendgrid import SendGrid


class Template(ModelSQL, ModelView):
    "Email Template"
    __name__ = 'email.template'
    name = fields.Char('Name', required=True)
    subject = fields.Char('Subject')
    html_body = fields.Text('HTML Body')
    from_email = fields.Char('From Email')
    recipients = fields.Char('Recipients', help='Mails separated by colon')
    attachment_filename = fields.Char('Attachment Filename')
    report = fields.Many2One('ir.action.report', 'Report')
    signature = fields.Boolean('Use Signature', help='The signature from the User details '
        'will be appened to the mail.')
    webapi = fields.Many2One('email.webapi', 'Web API', help='Web API send mail')
    bcc = fields.Char('BCC', help='Mails separated by colon')
    company = fields.Many2One('company.company', 'Company')

    @classmethod
    def __setup__(cls):
        super(Template, cls).__setup__()
        cls._buttons.update({
            'test_send': {
                'invisible': False,
            },
        })

    def pre_validate(self):
        super().pre_validate()
        mails = []
        if self.bcc:
            mails.extend(self.bcc.split(','))
        if self.recipients:
            mails.extend(self.recipients.split(','))
        if self.from_email:
            mails.append(self.from_email)
        if len(mails) != len(set(mails)):
            raise UserError(
                gettext('email.msg_duplicate_emails', emails=', '.join(mails)))

    @classmethod
    @ModelView.button
    def test_send(cls, records):
        for rec in records:
            if not rec.recipients:
                raise UserError(gettext('email.msg_no_recipients'))
            rec._test_send()

    def _test_send(self):
        webservice = self.webapi.web_service
        api = self.webapi.api_key
        provider = None
        if webservice == 'sendgrid':
            provider = SendGrid(api)
        elif webservice == 'mailtrap':
            provider = MailTrap(api)
        provider._test_send(self)

    @classmethod
    def send(cls, template, record, to_recipients=None, attach=False, attachments=[], body=None):
        if not template or (not to_recipients and not template.recipients):
            return
        webservice = template.webapi.web_service
        api = template.webapi.api_key
        provider = None
        if webservice == 'sendgrid':
            provider = SendGrid(api)
        elif webservice == 'mailtrap':
            provider = MailTrap(api)
        return provider.send(template, record, to_recipients, attach, attachments, body)

    @classmethod
    def engine_jinja2(cls, expression, record, api_response):
        """
        :param expression: Expression to evaluate
        :param record: Browse record
        """
        template_jinja = Jinja2Template(expression)
        return template_jinja.render({'record': record, 'api_response': api_response})

    def render_report(self, record):
        """Render the report and returns a tuple of data
        """
        Report = Pool().get(self.report.report_name, type='report')
        report = Report.execute([record.id], {'id': record.id})
        ext, data, filename, file_name = report
        return ext, data, filename, file_name

    def render(self, record):
        """Renders the template and returns as email object
        :param record: Browse Record of the record on which the template
            is to generate the data on
        :return: 'email.message.Message' instance
        """
        message = MIMEMultipart()
        message['date'] = formatdate(localtime=1)
        return message

    def render_and_send(self, records):
        """
        Render the template and send
        :param records: List Object of the records
        """
        return None


# class TemplateReport(ModelSQL, ModelView):

#     "Email Template Report"

#     __name__ = "email.template.report"

#     attachment_filename = fields.Char('Attachment Filename')
#     report = fields.Many2One('ir.action.report', 'Report')
#     template = fields.Many2One('email.template', 'Template')
